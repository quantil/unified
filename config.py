# Config file

# This config file contains important variables for the 
# execution and deployment of the web application

### APPS
app_alertas = "alertas"  
app_metricas = "metricas"

### DATOS
# metricas 
metricas_path_to_ex_ante = "exante_final.csv"
metricas_path_to_ex_post_p1 = "post_p1_final.csv"
metricas_path_to_ex_post_p1_p2 = "post_p1_p2_final.csv"

# alertas
alertas_path_to_exante = "exante_final.csv"
alertas_path_to_ex_post_p1 = "post_p1_final.csv"
alertas_path_to_ex_post_p1_p2 = ""

# nombres de columnas
marca_column_name = "reprobo"
nota_final_column_name = "calificacion_final"
class_column_name = "id_clase"
id_column_name = "id"
semester_column_name = "semestre"
basic_cols_to_keep = ['id', 'semestre', 'id_clase']


### MODELOS
ex_ante = "ex_ante"
ex_post_p1 = "ex_post_p1"
ex_post_p1_p2 = "ex_post_p1_p2"

best_thresholds = {
    "ex_ante": 0.5,
    "ex_post_p1": 0.6,
    "ex_post_p1_p2": 0.46,
}

probability_col_names = [
    "probabilidad_reprobar_exante",
    "probabilidad_reprobar_expost_p1",
    "probabilidad_reprobar_expost_p1_p2" ]

model_column_map = {
    "ex_ante": {
        "column_in_data": "probabilidad_reprobar_exante",
        "cols_to_ignore_if_missing": [
            "probabilidad_reprobar_exante",
            "probabilidad_reprobar_expost_p1",
            "probabilidad_reprobar_expost_p1_p2",
            "p1",
            "p2",
            marca_column_name,
            nota_final_column_name,
        ],
        "cols_to_check": [
            "probabilidad_reprobar_exante",
        ],
        "cols_available": [
            "id",
            "semestre",
            "id_clase",
            "probabilidad_reprobar_exante",
            "reprobo",
        ],
        "label": "Ex ante"
    },
    "ex_post_p1":{
        "column_in_data": "probabilidad_reprobar_expost_p1",
        "cols_to_ignore_if_missing": [
            "probabilidad_reprobar_expost_p1",
            "probabilidad_reprobar_expost_p1_p2",
            "p1",
            "p2",
            marca_column_name,
            nota_final_column_name,
        ],
        "cols_to_check": [
            "probabilidad_reprobar_exante",
            "probabilidad_reprobar_expost_p1",
            "p1",
        ],
        "cols_available": [
            "id",
            "semestre",
            "id_clase",
            "probabilidad_reprobar_exante",
            "probabilidad_reprobar_expost_p1",
            "p1",
            "reprobo",
        ],
        "label": "Ex post p1"
    },
    "ex_post_p1_p2":{
        "column_in_data": "probabilidad_reprobar_expost_p1_p2",
        "cols_to_ignore_if_missing": [
            "probabilidad_reprobar_expost_p1_p2",
            "p2",
            marca_column_name,
            nota_final_column_name,
        ],
        "cols_to_check": [
            "probabilidad_reprobar_exante",
            "probabilidad_reprobar_expost_p1",
            "p1",
            "probabilidad_reprobar_expost_p1_p2",
            "p2",
            marca_column_name,
            nota_final_column_name,
        ],
        "cols_available": [
            "id",
            "semestre",
            "id_clase",
            "probabilidad_reprobar_exante",
            "probabilidad_reprobar_expost_p1",
            "p1",
            "reprobo",
            "probabilidad_reprobar_expost_p1_p2",
            "p2"
        ],
        "label": "Ex post p1 p2"
    }
}

class_column_map = {
    'FISI-1018':{
        "value": "FISI-1018",
        "label": "Física 1"
    },
    'FISI-1028':{
        "value": "FISI-1028",
        "label": "Física 2"
    },
    'MATE-1105':{
        "value": "MATE-1105",
        "label": "Álgebra Lineal 1"
    },
    "MATE-1203": {
        "value": "MATE-1203",
        "label": "Cálculo Diferencial"
    },
    "MATE-1207":{
        "value": "MATE-1207",
        "label": "Cálculo Vectorial"
    },
}

def get_columns_for_available_models(available_models):
    """ Generates a tuple that contains:
    1. a list with all the column names that should be
    available according to the models
    2. a list containing the human names of those columns
    """
    columns = []
    human_names = []
    for model in available_models:
        columns.append(model_column_map[model]["column_in_data"])
        human_names.append(model_column_map[model]["label"])
    
    return columns, human_names

def generate_models_available_dropdown(available_models):
    """ Generates a list to feed a dash dropdown
    containing all the available models """
    dropdown = []
    for model in available_models:
        dropdown.append(
            {
                "label": model_column_map[model]["label"],
                "value": model
            }
        )
    return dropdown

def get_value_from_available_models(available_models):
    value = None
    if len(available_models) > 0:
        value = available_models[0]
    return  value

def generate_classes_available_dropdown(available_classes):
    """ Generates a list to feed a dash dropdown
    containing all the available classes """
    dropdown = []
    for class_value in available_classes:
        dropdown.append(
            {
                "label": class_column_map[class_value]["label"],
                "value": class_value
            }
        )
    return dropdown

def get_value_from_available_classes(available_classes):
    value = None
    if len(available_classes) > 0:
        value = available_classes[0]
    return  value


### ALERTAS
# classes available alertas
current_semester_alertas =[
                        {'label': '201820', 'value': '201820'},
                    ]

classes_available_alertas=['FISI-1018', 'FISI-1028', 'MATE-1105', 'MATE-1203', 'MATE-1207']

# models available alertas
models_available_alertas = ["ex_ante", "ex_post_p1",]
models_not_available_alertas= ["ex_post_p1_p2"]



### METRICAS
# classes available metricas
current_semester_metricas = options=[
    {'label': '201810', 'value': '201810'},
    {'label': '201820', 'value': '201820'},
    ]

classes_available_metricas = ['FISI-1018', 'FISI-1028', 'MATE-1105', 'MATE-1203', 'MATE-1207']
classes_not_available_metricas = []

# models available metricas
models_available_metricas=["ex_ante", "ex_post_p1", "ex_post_p1_p2"]


### DESCARGA DE DATOS

prefijo_csv_archivo_alertas = "estudiantes_alertas"
prefijo_csv_archivo_metricas = "estudiantes_metricas"


# OTROS
# posibles nombres para los títulos de las gráficas
figure_types = {
    "roc":{
        "value": "roc",
        "title": "ROC Curve"
    },
    "pr": {
        "value": "pr",
        "title": "Precision-Recall"
    },
    "hs":{
        "value": "hs",
        "title": "Grade Distribution"
    },
    "cmp":{
        "value": "cmp",
        "title": "Confusion Matrix (Pie Chart)"
    },
    "cmhm":{
        "value": "cmp",
        "title": "Confusion Matrix"
    }
}


## Errores posibles de train vs test dataP
check_type_errors = {
    "different_length": {
        "value": "different_length",
        "message": """Los vectores necesarios para el gráfico y_pred,
        y_test tienen diferente longitud"""
    },
    "one_vector_has_length_zero": {
        "value": "one_vector_has_length_zero",
        "message": """y_pred, y_test longitud es 0 (cero)"""
    }
}

## Columnas en lenguaje natural
human_column_names = {
    "id": "ID Estudiante",
    "semestre": "Semestre",
    "id_clase": "ID clase",
    "probabilidad_reprobar_exante": "Pr exante", 
    "probabilidad_reprobar_expost_p1" : "Pr expost P1",
    "p1": "Parcial 1",
    "probabilidad_reprobar_expost_p1_p2": "Pr expost P1 P2",
    "p2": "Parcial 2",
    "reprobo": "Reprobó",
    "calificacion_final": "Calificación Final"
}


## estilos de las tablas
n_fixed_rows=1,
style_table_failing_students={
    'overflowX': 'scroll',
    'overflowY': 'scroll',
    'maxHeight': '400px',
    'border': 'thin lightgrey solid',
},
style_table_all_grades={
    'overflowX': 'scroll',
    'overflowY': 'scroll',
    'maxHeight': '400px',
    'border': 'thin lightgrey solid',
},
style_cell={
    'width': '200px',
    'whiteSpace': 'normal'
},
style_header={
    'backgroundColor': 'white',
    'fontWeight': 'bold'
},
style_cell_conditional=[{
    'if': {'row_index': 'odd'},
    'backgroundColor': 'rgb(248, 248, 248)'
}],
css=[{
    'selector': '.dash-cell div.dash-cell-value',
    'rule': 'display: inline; white-space: inherit; overflow: inherit; text-overflow: inherit;'
}]