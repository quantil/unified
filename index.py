import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

from app import app
from apps import alertas, metricas

from app import server

app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content')
])


def display_menu():
    return html.Div(children=[
        html.Div(className="banner", children=[
            # Change App Name here
            html.Div(className='container scalable', children=[
                # Change App Name here
                html.H2('Modelo de Alertas'),

                html.A(
                    html.Img(src=app.get_asset_url("images/uniandes-logo.png")),
                    href='https://ciencias.uniandes.edu.co/'
                )
            ]),
        ]),
        html.Div(className="container", children=[
            html.Div(children=[
                html.H3(children=[
                    "Objetivo"
                ]),
                html.P(children=[
                    """Recientemente la Facultad de Ciencias, con el apoyo financiero del ICFES, realizó una investigación
                    sobre factores que afectan el desempeño académico de los estudiantes
                    de cursos del ciclo básico de la Faculta de Ciencias de la Universidad de los Andes.
                    Como resultado de este estudio (véase Rodriguez et al (2018) y el reporte: Predicción
                    del desempeño académico usando técnicas de aprendizaje de máquinas, Octubre 25
                    de 2018) en este documento se propone implementar este modelo de alertas como un
                    prototipo de herramienta (software) para que pueda ser usado por la Facultad de
                    Ciencias de forma eficiente y práctica para generar alertas d ellos estudiantes que se
                    encuentran en riesgo de perder alguna de las materias del ciclo básico"""
                ])
            ]),
            dcc.Link(html.Button('Alertas: Generación de alertas para semestre actual'), href='/apps/alertas'),
            dcc.Link(html.Button('Métricas de desempeño de los modelos en semestres anteriores'), href='/apps/metricas'),
                
        ]), 
    ])


@app.callback(Output('page-content', 'children'),
              [Input('url', 'pathname')])
def display_page(pathname):
    if pathname == "/":
        return display_menu()
    elif pathname == '/apps/alertas':
         return alertas.layout
    elif pathname == '/apps/metricas':
         return metricas.layout
    else:
        return display_menu()


local_css = [
    # Normalize the CSS
    app.get_asset_url("css/normalize.css"),
    # Fonts
    app.get_asset_url("css/Roboto"),
    app.get_asset_url("css/font-awesome.min.css"),
    # Base Stylesheet
    app.get_asset_url("css/base-styles.css"),
    # Custom Stylesheet
    app.get_asset_url("css/custom-styles.css"),
    # loading state
    app.get_asset_url("css/loading-state.css"),

]
external_css = [
    # Normalize the CSS
    "https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css",
    # Fonts
    "https://fonts.googleapis.com/css?family=Open+Sans|Roboto",

    "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css",
    # Base Stylesheet, replace this with your own base-styles.css using Rawgit
    "https://rawgit.com/xhlulu/9a6e89f418ee40d02b637a429a876aa9/raw/f3ea10d53e33ece67eb681025cedc83870c9938d/base-styles.css",
    # Custom Stylesheet, replace this with your own custom-styles.css using Rawgit
    "https://cdn.rawgit.com/plotly/dash-svm/bb031580/custom-styles.css"
]

for css in local_css:
    app.css.append_css({"external_url": css})

if __name__ == '__main__':
    app.run_server(debug=True)

#https://codepen.io/chriddyp/pen/bWLwgP.css