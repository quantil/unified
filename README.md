# Generación de alertas y métricas

Este repositorio contiene un **prototipo** de herramienta (software) que permite a la Facultad de Ciencias de la Universidad de los Andes:

1. Generar alertas de los estudiantes que se encuentran en riesgo de perder alguna de las materias del ciclo básico para el semestre actual, además de obtener métricas del desempeno de los estudiantes con el fin de tomar acciones preventivas.

2. Evaluar el desempeño de los estudiantes en los semestres anteriores, el riesgo que tenían de perder alguna de las materias del ciclo básico para el semestre actual y finalmente obtener métricas del desempeno. Esto se hace con el fin de obtener insights que lleven a la toma de decisiones correctivas



## Tabla de contenidos

- [Arquitectura](#arquitecture)
- [Instalación](#installation)
- [Datos](#data)
- [Despliegue](#deployment)
- [Uso](#usage)
- [Mantenimiento](#maintenance)
- [Soporte](#support)
- [Contribuciones y comentarios](#contributing)
- [Licencias](#licensing)

## Arquitecture 

Esta aplicación recibe los datos emitidos por el pipeline y flujo de datos escrito R anteriormente. Los datos deben ser ublicados en el directorio raíz. Si se desea ubicar la data en otro lugar, ver la sección data de este documento.

Dadas las dos funcionalidades descritas arriba (generación de alertas, métricas de desempeño), esta aplicación está compuesta por dos componentes principales, cada uno asociado a una funcionalidad:

1. Generación de alertas: `app/alertas.py`
2. Métricas de desempeño: `app/metricas.py`

Hay también otros componentes que soportan la operación de ambas aplicaciones. Estos son:

1. Ejecutador de la aplicación: `index.py` - Este ejecutador es el que permite ejecutar la aplicación usando Python. Este contiene a la app de métricas y a la app de métricas. A su vez provee un punto de inicio que le permite al usuario navegar entre las dos aplicaciones.
2. App: `app.py` - Contiene la instancia app que `index.py` usa para contener las aplicaciones. 
3. Configuración de la aplicación: `config.py` - Contiene las variables de configuración que la aplicación usa para funcionar adecuadamente. Es FUNDAMENTAL que este archivo sea correcto. 
4. Módulo para operar data - `utilities/data.py`
5. Módulo para construir figuras - `utilities/figures.py`
6. Módulo para calcular estadísticas - `utilities/statistics.py` 

Como se puede ver se procuró modularizar lo más posible, dejando el código cohesivo y con poco acoplamiento.

Es preciso asegurarse que todos estos módulos estén presentes para que la aplicación funcione correctamente.

## Installation

Para que el prototipo funcione el servidor de despliegue requiere Python 3.6.5 junto con varias dependencias. Para la instalación de Python hay varios métodos como Anaconda, virtualenvs, entre otros. Nosotros sugerimos usar pyenv. Para instalar Pyenv puede seguir la guía de instalación disponible en [Tecnmint](https://www.tecmint.com/pyenv-install-and-manage-multiple-python-versions-in-linux/)


Una vez Python haya sido instalado y el ambiente de instalación con Python 3.6.5 activado, debe instalar los requerimientos para que la aplicación funcione. Para esto debe ejecutar, una vez ubicado en el directorio raíz del proyecto:

```bash
pip install -r requirements.txt
```

Una vez las dependencias estén configuradas, debe asegurarse de que el archivo `config.py` esté en la raíz del proyecto y que a su vez tenga las variables inicializadas con sus valores correctos de acuerdo con las necesidades de la facultad.

## Data

Los datos actualmente se encuentran en el directorio raíz del proyecto. En caso de que se quieran mover a otra locación dentro fuera del proyecto se puede hacer, pero se deben cambiar ciertas variables en `config.py`: `metricas_path_to_ex_ante`, `metricas_path_to_ex_post_p1`,
`metricas_path_to_ex_post_p1_p2`, `alertas_path_to_exante`, `alertas_path_to_ex_post_p1` y `alertas_path_to_ex_post_p1_p2`. 

Los datos que alimentan la aplicación de alertas y la aplicación de métricas deberían ser diferentes, pero por ahora, dado que no tenemos datos del semestre más reciente/actual (201910), los datos que alimentan ambas aplicaciones son los mismos. En el futuro estos deberán ser diferentes. Lo importante en cuanto a los datos es que estén bien referenciados en las variables de arriba en el archivo `config.py`.


## Como son los datasets

La estructura de los datos es fundamental para que la aplicación funcione bien. Si los datos no tienen la estructura definida a continuación para las tablas de cada modelo la aplicación podría funcionar de formas extrañas o fallar completamente.

### Ex ante
Columnas | Ejemplo valor
------------ | -------------
id| 201462918
semestre | 201820
id_clase | MATE-1203
probabilidad_reprobar_exante | 0.7865
reprobo | 1


### Ex post Parcial 1

Columnas | Ejemplo valor
------------ | -------------
id| 201462918
semestre | 201820
id_clase | MATE-1203
probabilidad_reprobar_exante | 0.7865
reprobo | 1
probabilidad_reprobar_expost_p1 | 0.9126
p1 | 2.23

### Ex post Parcial 1 y 2

Columnas | Ejemplo valor
------------ | -------------
id| 201462918
semestre | 201820
id_clase | MATE-1203
probabilidad_reprobar_exante | 0.7865
reprobo | 1
probabilidad_reprobar_expost_p1 | 0.9126
p1 | 2.23
probabilidad_reprobar_expost_p1_p2 | 0.9526
p2 | 4.07

Independientemente de sean los datos para la aplicación de alertas o para la aplicación de métricas, el formato para ambas aplicaciones es el mismo.

## Deployment

Antes de hacer un despliegue es preciso verificar que los datos existan y tengan el formato adecuado. Recomendamos ver la sección anterior de data para saber qué datos espera la aplicación y dónde espera encontrarlos.

Si se quiere ejecutar la aplicación en desarrollo basta con ejecutar el comando

```
python index.py
```
Vale mencionar que si se despliega un servidor de esta forma, este sólo podrá ser accedido localmente, no desde clientes externos. Para esto se debe hacer un despliegue en producción.

Si se quiere ejecutar la aplicación para un despliegue en producción se debe usar [gunicorn](https://gunicorn.org/). gunicorn es un servidor HTTP Python con soporte del estándar WSGI para unix. Este permite desplegar aplicaciones que escalen usando un modelo basado en workers. Para ejecutar la aplicación usando gunicorn hay dos opciones:

1. Ejecutar el Procfile que se encuentra en el directorio raíz.

```
bash Procfile
```

2. Ejecutar el comando que invoca gunicorn directamente desde la terminal

```
gunicorn index:server -b :80  -w 4
```

Este comando conecta el servidor de despliegue gunicorn con el puerto 80 y a su configura el servidor para que sea desplegado con 4 workers. 

## Files

Esta aplicación fue construida con Dash.  Dash es un framework de desarrollo productivo de Python para crear aplicaciones web.

Dash, escrito sobre Flask, Plotly.js y React.js, es ideal para crear aplicaciones de visualización de datos con interfaces de usuario altamente personalizadas en Python puro. Es particularmente adecuado para cualquier persona que trabaje con datos en Python.

A través de un par de patrones simples, Dash abstrae todas las tecnologías y protocolos necesarios para construir una aplicación interactiva basada en la web. Dash es lo suficientemente simple como para vincular una interfaz de usuario alrededor de su código Python en una tarde.

Las aplicaciones de Dash se renderizan en el navegador web. Puede implementar sus aplicaciones en servidores y luego compartirlas a través de URL. Dado que las aplicaciones de Dash se visualizan en el navegador web, Dash es intrínsecamente compatible con múltiples plataformas y dispositivos móviles.

En esta aplicación se intentó hacer uso de las mejores prácticas disponibles explicadas en la documentación del framework. Puede acceder a esta documentación  [aquí](https://dash.plot.ly/).

### Archivos de servidor `config.py` y `app.py`


### Archivo de servidor `config.py`

Como se menciona a lo largo de este documento, el archivo `config.py` contiene todas las variables de configuración que la aplicación necesita para funcionar adecuadamente.  Es FUNDAMENTAL que las variables aquí tengan sentido y sean coherentes con los datos que toma la aplicación para funcionar. A continuación se presentan las secciones de `config.py`  y luego  los pasos a seguir para configurar adecuadamente las variables para que la app funcione.

####  Secciones de `config.py` 
1. APPS
2. DATOS
3. MODELOS
4. ALERTAS
5. METRICAS
6. DESCARGA DE DATOS
7. OTROS

#### Configuración adecuada de `config.py` 
1. En la sección `DATOS` asegurarse que las variables con los path a los archivos sean correctos. 
2. En la sección `DATOS`  verificar que los nombres de las columnas coincidan con los de los datos que se van a importar. Si son diferentes, recomendamos usar R o Python o su herramienta/lenguaje de preferencia para cambiar los nombres de las columnas.
3.   En la sección `MODELOS`  verificar que los nombres en la lista `probability_col_names` correspondan a los de los datos a importar. Si son diferentes, recomendamos usar R o Python o su herramienta/lenguaje de preferencia para cambiar los nombres de las columnas.
4. En la sección `MODELOS` está la variable `class_column_map`. Esta variable es un diccionario con las clases disponibles en los datos a insertar. ya existen unas clases agregadas a la fecha (Julio 2019) pero estas clases pueden variar. Asegurarse de que las clases necesarias estén en ese diccionario.
5. En la sección `ALERTAS` verificar `current_semester_alertas`. Este semestre debe corresponder con el semestre actual o con el semestre con las notas procesadas más reciente. Por ahora está en 201820. A su vez asegurarse que `classes_available_alertas` tenga sólo las clases disponibles. `models_available_alertas` debe tener sólo los modelos disponibles. Está también `models_not_available_alertas`, pero esta variables sólo se deja para hacer notar que es posible que para alertas no haya un modelo disponible. Esto se debe a que aún no se ha llegado temporalmente a la etapa del semestre para poder correr un modelo particular, pero si se pueden correr otros.

6. En la sección `METRICAS` verificar `current_semester_metricas`. Esta variable debe contener los semestres para los cuales ya hay notas disponibles. Por ahora está 201810 y 201820. A su vez asegurarse que `classes_available_metricas` tenga sólo las clases disponibles. `models_available_metricas` debe tener sólo los modelos disponibles. Todos los modelos deben estar disponibles para que los datos aparezcan en métricas.


## Maintenance

### En caso de tener problemas con el despligue (errores, tiempo de carga, tiempo de respuesta)
1. Asegurarse que el `config.py` este configurado adecuadamente. Para esto referirse a la sección de instalación donde se explica a profundidad `config.py` y sus variables. 

2. Asegurarse que el servidor de despliegue tenga las especificaciones necesarias para soportar la carga que esté recibiendo el servidor

3. Asegurarse que el número de workers sea el apropiado. Según la documentación de [gunicorn](http://docs.gunicorn.org/en/stable/design.html#how-many-workers), No escale la cantidad de workers basado en la cantidad de clientes que espera tener. Gunicorn solo debería necesitar 4-12 worker processes para manejar cientos o miles de solicitudes por segundo. Gunicorn confía en el sistema operativo para proporcionar todo el equilibrio de carga cuando se manejan las solicitudes. Generalmente recomienda `(2 x $num_cores) + 1` como el número de workers para empezar. Dado esto, identifique el número de cores de su servidor de despliegue e indique en el script de despliegue en producción (Procfile o terminal) el número de workers que necesita.


## Support

Para soporte contactar a [Quantil](http://quantil.co), específicamente a Álvaro Riascos.

## Contributing

Por favor envía sus sugerencias y comentarios a [Facultad de ciencias](mailto:facultadciencias@uniandes.edu.co)

## Licensing

Todo el software en este repositorio pertenece a la facultad de Ciencias de la Universidad de los Andes