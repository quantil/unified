
# -*- coding: utf-8 -*-
import base64
from datetime import datetime
import dash
from dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html
import dash_table
import flask
import io
import pandas as pd
import plotly.plotly as py
from plotly import graph_objs as go

from utilities.figures import serve_pie_confusion_matrix,\
    serve_roc_curve, serve_random_roc_curve, serve_overlaid_histogram, \
    serve_random_figure, serve_precision_recall_curve, serve_histogram, \
    serve_histogram_v2
import utilities.data as data
from app import app
import config

data_tables =  {
    "ex_ante": pd.read_csv(config.alertas_path_to_exante),
    "ex_post_p1": pd.read_csv(config.alertas_path_to_ex_post_p1),
}

    
    
layout = html.Div(children=[
    # .container class is fixed, .container.scalable is scalable
    html.Div(className="banner", children=[
        # Change App Name here
        html.Div(className='container scalable', children=[
            # Change App Name here
            html.H2('Modelo de Alertas'),

            html.A(
                html.Img(src=app.get_asset_url("images/uniandes-logo.png")),
                href='https://ciencias.uniandes.edu.co/'
            )
        ]),
    ]),

    html.Div(id='body', className='container scalable', children=[
        dcc.Link(html.Button('Atrás'), href='/'),
        html.Div(children=[
            html.Div([
                html.Label('Escoge el semestre'),
                dcc.Dropdown(
                    id="semester_selector_alertas",
                    options=config.current_semester_alertas, 
                    value=config.current_semester_alertas[0]["value"],
                    style={'position':'relative', 'zIndex':'1000'}
                ),
                html.Label('Escoge la materia'),
                dcc.Dropdown(
                    id="class_name_selector_alertas",
                    options=config.generate_classes_available_dropdown(config.classes_available_alertas),
                    value=config.get_value_from_available_classes(config.classes_available_alertas),
                    style={'position':'relative', 'zIndex':'999'}
                ),
            ], className="six columns"),
            html.Div([
                html.Label('Escoge el modelo'),
                dcc.Dropdown(
                    id ="model_selector_alertas",
                    options=config.generate_models_available_dropdown(config.models_available_alertas),
                    value=config.get_value_from_available_models(config.models_available_alertas),
                    style={'position':'relative', 'zIndex':'999'}
                ),
                html.Label('Seleccione el Threshold'),
                dcc.Input(
                    id='threshold_input_alertas',
                    type='number',
                    value=0.5,
                ),
                dcc.Slider(
                    id="threshold_slider_alertas",
                    min=0,
                    max=1,
                    step=0.01,
                    value=0.5,
                ),
                html.Div(id='slider_output_container_alertas'),
            ], className="six columns"),
        ], className = "row"),
        html.Div(children=[
            html.H4('Búsqueda individual:', className="twelve columns"),
            html.Div(children=[
                html.Label('Escribe el ID del estudiante cuya resultado quieres ver para la materia, modelo y semestre actuales:'),
                dcc.Input(
                    id='id_student_alertas',
                    type='number',
                    value=1
                ),
                html.Div(children=[
                    dash_table.DataTable(
                    id='table_individual_student_alertas',
                    columns=data.get_random_cols(),
                    data=data.get_random_data(),
                    n_fixed_rows=1,
                    style_table={
                        'overflowX': 'scroll',
                        'overflowY': 'scroll',
                        'maxHeight': '200px',
                        'border': 'thin lightgrey solid',
                    },
                    style_cell={
                        'width': '200px',
                        'whiteSpace': 'normal'
                    },
                    style_header={
                        'backgroundColor': 'white',
                        'fontWeight': 'bold'
                    },
                    style_cell_conditional=[{
                        'if': {'row_index': 'odd'},
                        'backgroundColor': 'rgb(248, 248, 248)'
                    }],
                    css=[{
                        'selector': '.dash-cell div.dash-cell-value',
                        'rule': 'display: inline; white-space: inherit; overflow: inherit; text-overflow: inherit;'
                    }]
                    )
                ],className="twelve columns")
                
                
            ],className="row"),
        ], className="row"),
        html.Div(children=[
            html.H4('Estudiantes que reprueban:',id='number_of_students_alertas', className="twelve columns"),

            html.Div(children=[
                dash_table.DataTable(
                id='table_filtered_alertas',
                columns=data.get_random_cols(),
                data=data.get_random_data(),
                n_fixed_rows=1,
                style_table={
                    'overflowX': 'scroll',
                    'overflowY': 'scroll',
                    'maxHeight': '400px',
                    'border': 'thin lightgrey solid',
                },
                style_cell={
                    'width': '200px',
                    'whiteSpace': 'normal'
                },
                style_header={
                    'backgroundColor': 'white',
                    'fontWeight': 'bold'
                },
                style_cell_conditional=[{
                    'if': {'row_index': 'odd'},
                    'backgroundColor': 'rgb(248, 248, 248)'
                }],
                css=[{
                    'selector': '.dash-cell div.dash-cell-value',
                    'rule': 'display: inline; white-space: inherit; overflow: inherit; text-overflow: inherit;'
                }]
                )
            ], className="twelve columns"),
        ], className="row"),

        html.Div(children=[
            dcc.ConfirmDialogProvider(children=html.Button('Generar CSV de alerta con estudiantes con alta probabilidad de reprobar',
                id='generate_file_button_alertas', className="six columns", title="botón de generación de archivo", style={'backgroundColor':'#FC8600', 'color': 'white'} ),
                id='generate_new_link_provider_alertas',
                message="""El CSV con los estudiantes en riesgo ha sido generado.
                Presiona el botón azul descargar CSV para descargar el archivo en su equipo local"""),
            html.A(html.Button(children=['Descargar CSV generado'], id = 'boton_download_alertas', style={'backgroundColor':'#119DFF', 'color': 'white'}),
            id='my_link_alertas', className="six columns"),
        ], className="row"),
        

        html.Div(children=[
            html.H4('Todos los estudiantes:',id="number_of_students_all_grades_alertas" , className="twelve columns"),

            html.Div(children=[
                dash_table.DataTable(
                id='table_alertas',
                columns=data.get_random_cols(),
                data=data.get_random_data(),
                n_fixed_rows=1,
                style_table={
                    'overflowX': 'scroll',
                    'overflowY': 'scroll',
                    'maxHeight': '400px',
                    'border': 'thin lightgrey solid',
                },
                style_cell={
                    'width': '200px',
                    'whiteSpace': 'normal'
                },
                style_header={
                    'backgroundColor': 'white',
                    'fontWeight': 'bold'
                },
                style_cell_conditional=[{
                    'if': {'row_index': 'odd'},
                    'backgroundColor': 'rgb(248, 248, 248)'
                }],
                css=[{
                    'selector': '.dash-cell div.dash-cell-value',
                    'rule': 'display: inline; white-space: inherit; overflow: inherit; text-overflow: inherit;'
                }]
                )
            ], className="twelve columns"),
        ], className="row"),

        html.Div(children=[
            html.H4('Histograma con probabilidad de reprobar' , className="twelve columns"),

        ], className="row"),        

        html.Div(children=[
            html.Div(children=[
                dcc.Graph(
                    id='probabilities_histogram_alertas',
                    figure=serve_random_figure(),
                ),
            ], className="twelve columns"),
        ], className="Row"),
    ])    
])

# callbacks

# slider
@app.callback(
    dash.dependencies.Output('slider_output_container_alertas', 'children'),
    [dash.dependencies.Input('threshold_slider_alertas', 'value')])
def update_output_threshold_alertas(value):
    return """El threshold de probabilidad para perder es "{}".
      Se seleccionarán los estudiantes con una probabilidad
      de perder superior al threshold.
     """.format(value)

@app.callback(
    dash.dependencies.Output('threshold_slider_alertas', 'value'),
    [dash.dependencies.Input('threshold_input_alertas', 'value')])
def update_value_slider_alertas(value):
    return min(float(value), 1)

# individual search
# filtered data 
@app.callback(
    [dash.dependencies.Output('table_individual_student_alertas', 'columns'),
    dash.dependencies.Output('table_individual_student_alertas', 'data'),],
    [dash.dependencies.Input('id_student_alertas', 'value'),
    dash.dependencies.Input('semester_selector_alertas', 'value'),
    dash.dependencies.Input('class_name_selector_alertas', 'value'),
    dash.dependencies.Input('model_selector_alertas', 'value'),])
def update_individual_student_result(id_student, semester, class_name, model):
    data_table = data_tables[model]
    filtered_data = data.get_data_for_single_student(data_table, id_student, semester, class_name)
    human_cols_in_order, data_with_columns_in_order = data.select_only_available_cols_v2(filtered_data, config.app_metricas, model)
    data_table_cols = [{"name": i["human_name"], "id": i["id"]} for i in human_cols_in_order]
    return data_table_cols, data_with_columns_in_order.to_dict("rows")




#  update # of students and filter table
@app.callback(
    [dash.dependencies.Output('number_of_students_alertas', 'children'),
    dash.dependencies.Output('table_filtered_alertas', 'columns'),
    dash.dependencies.Output('table_filtered_alertas', 'data'),],
    [dash.dependencies.Input('threshold_slider_alertas', 'value'),
    dash.dependencies.Input('model_selector_alertas', 'value'),
    dash.dependencies.Input('class_name_selector_alertas', 'value'),
    dash.dependencies.Input('semester_selector_alertas', 'value'),])
def update_number_students_and_filter_data_table_alertas(threshold, model, class_name, semester):
    data_table = data_tables[model]
    filtered_data = data.filter_student_data(data_table, threshold, model, class_name, semester)
    human_cols_in_order, data_with_columns_in_order = data.select_only_available_cols_v2(filtered_data, config.app_metricas, model)
    number = data_with_columns_in_order.shape[0]
    number_message = 'Estudiantes que reprueban: ' + str(number)
    data_rows = data_with_columns_in_order.to_dict("rows")
    data_table_cols = [{"name": i["human_name"], "id": i["id"]} for i in human_cols_in_order]
    return  number_message,data_table_cols, data_rows

@app.callback(
    [dash.dependencies.Output('number_of_students_all_grades_alertas', 'children'),
    dash.dependencies.Output('table_alertas', 'columns'),
    dash.dependencies.Output('table_alertas', 'data'),],
    [dash.dependencies.Input('model_selector_alertas', 'value'),
    dash.dependencies.Input('class_name_selector_alertas', 'value'),
    dash.dependencies.Input('semester_selector_alertas', 'value'),])
def update_all_grades_alertas(model, class_name, semester):
    data_table = data_tables[model]
    filtered_data = data.filter_student_data_all_grades(data_table, class_name, semester)
    human_cols_in_order, data_with_columns_in_order = data.select_only_available_cols_v2(filtered_data, config.app_metricas, model)
    number = data_with_columns_in_order.shape[0]
    number_message = 'Todos los estudiantes: ' + str(number)
    data_rows = data_with_columns_in_order.to_dict("rows")
    data_table_cols = [{"name": i["human_name"], "id": i["id"]} for i in human_cols_in_order]
    return  number_message,data_table_cols, data_rows

@app.callback(
    dash.dependencies.Output('probabilities_histogram_alertas', 'figure'),
    [dash.dependencies.Input('semester_selector_alertas', 'value'),
    dash.dependencies.Input('class_name_selector_alertas', 'value')])
def update_histogram_alertas(semester, class_name):
    filtered_tables = {}
    for model, student_data_table in data_tables.items():
        filtered_data = data.filter_student_data_all_grades(student_data_table, class_name, semester)
        filtered_tables[model] = filtered_data
    histogram = serve_histogram_v2(filtered_tables, config.app_alertas)
    return histogram


# boton descarga CSV

@app.callback(
    Output('my_link_alertas', 'href'),
    [dash.dependencies.Input('generate_new_link_provider_alertas', 'submit_n_clicks')],
    [dash.dependencies.State('threshold_slider_alertas', 'value'),
    dash.dependencies.State('model_selector_alertas', 'value'),
    dash.dependencies.State('class_name_selector_alertas', 'value'),
    dash.dependencies.State('semester_selector_alertas', 'value')])
def update_link(n_clicks, threshold, model, class_name, semester):
    value = str(threshold) + "$" + str(model) + "$" + str(class_name) + "$" + str(semester)
    urlSafeEncodedBytes = base64.urlsafe_b64encode(value.encode("utf-8"))
    urlSafeEncodedStr = str(urlSafeEncodedBytes, "utf-8")
    link_to_return = '/dash/urlToDownloadAlertas?value={}'.format(urlSafeEncodedStr)
    return link_to_return


@app.server.route('/dash/urlToDownloadAlertas') 
def download_csv_alertas():
    value = flask.request.args.get('value')
    # create a dynamic csv or file here using `StringIO` 
    # (instead of writing to the file system)
    value_decoded = base64.urlsafe_b64decode(value).decode("utf-8", "ignore") 
    params = value_decoded.split("$")
    threshold = float(params[0])
    model = params[1]
    class_name = params[2]
    semester = params[3]
    data_table = data_tables[model]
    filtered_data = data.filter_student_data(data_table, threshold, model, class_name, semester)
    human_cols_in_order, data_with_columns_in_order = data.select_only_available_cols_v2(filtered_data, config.app_metricas, model)
    str_io = io.StringIO()
    data_with_columns_in_order.to_csv(str_io)
    mem = io.BytesIO()
    mem.write(str_io.getvalue().encode('utf-8'))
    mem.seek(0)
    str_io.close()
    now = datetime.now()
    d = now.strftime("%d-%m-%Y-%H:%M:%S")
    return flask.send_file(mem,
                     mimetype='text/csv',
                     attachment_filename= config.prefijo_csv_archivo_alertas + "-" + d +'.csv',
                     as_attachment=True)


@app.callback(
    dash.dependencies.Output('boton_download_alertas', 'children'),
    [dash.dependencies.Input('generate_new_link_provider_alertas', 'submit_n_clicks')])
def update_output_button_alertas(n_clicks):
    now = datetime.now()
    d = now.strftime("%d-%m-%Y-%H:%M:%S")
    return 'Descargar CSV generado a las ' + d





