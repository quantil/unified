# -*- coding: utf-8 -*-
import base64
from datetime import datetime
import dash
from dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html
import dash_table
import flask
import io
import pandas as pd
import plotly.plotly as py
from plotly import graph_objs as go

from utilities.figures import serve_pie_confusion_matrix,\
    serve_roc_curve, serve_random_roc_curve, serve_overlaid_histogram, \
    serve_random_figure, serve_precision_recall_curve, serve_histogram, \
    serve_heatmap_confusion_matrix, serve_histogram_v2
import utilities.data as data
import utilities.statistics as statistics
from app import app
import config

data_tables =  {
    "ex_ante": pd.read_csv(config.metricas_path_to_ex_ante),
    "ex_post_p1": pd.read_csv(config.metricas_path_to_ex_post_p1),
    "ex_post_p1_p2": pd.read_csv(config.metricas_path_to_ex_post_p1_p2)
}

#models in general
figures_for_models = {}
for model_global in config.models_available_metricas:
    figures_for_models[model_global] = {}
    filter_column_model = data.get_filter_column(model_global)
    data_table = data_tables[model_global]
    y_test_model = data_table[config.marca_column_name]
    y_pred_model = data_table[filter_column_model]
    best_threshold = config.best_thresholds[model_global]
    hmcm_local = serve_heatmap_confusion_matrix(y_test_model, y_pred_model, best_threshold)
    gpcm_local = serve_pie_confusion_matrix(y_test_model, y_pred_model, best_threshold)
    roc_local = serve_roc_curve(y_pred_model, y_test_model)
    pr_local = serve_precision_recall_curve(y_test_model, y_pred_model, best_threshold)
    statistic, p_value = statistics.get_ks_test_2samp(y_pred_model, y_test_model)
    s = ("{0:.3f}".format(statistic) , "{0:.3f}".format(p_value))
    figures_for_models[model_global]["hmcm"] = hmcm_local
    figures_for_models[model_global]["gpcm"] = gpcm_local
    figures_for_models[model_global]["roc"] = roc_local
    figures_for_models[model_global]["pr"] = pr_local
    figures_for_models[model_global]["ks"] = s
    


layout =  html.Div(children=[
    # .container class is fixed, .container.scalable is scalable
    html.Div(className="banner",
    # this style changes the color of the banner for app2 (metrics)
    style={'background-color': '#889eb5'}, 
    children=[
        # 
        # Change App Name here
        html.Div(className='container scalable', children=[
            # Change App Name here
            html.H2('Modelo de Alertas - Métricas'),

            html.A(
                html.Img(src=app.get_asset_url("images/uniandes-logo.png")),
                href='https://ciencias.uniandes.edu.co/'
            )
        ]),
    ]),

    html.Div(id='body', className='container scalable', children=[
        dcc.Link(html.Button('Atrás'), href='/'),
        html.Div(children=[
            html.Div([
                html.Label('Escoge el semestre'),
                dcc.Dropdown(
                    id="semester_selector",
                    options=config.current_semester_metricas,
                    value=config.current_semester_metricas[-1]['value'],
                    style={'position':'relative', 'zIndex':'1000'}
                ),

                html.Label('Escoge la materia'),
                dcc.Dropdown(
                    id ="class_name_selector",
                    options=config.generate_classes_available_dropdown(config.classes_available_metricas),
                    value=config.get_value_from_available_classes(config.classes_available_metricas),
                    style={'position':'relative', 'zIndex':'999'}
                ),
            ], className="six columns"),
            html.Div([
                html.Label('Escoge el modelo'),
                dcc.Dropdown(
                    id ="model_selector",
                    options=config.generate_models_available_dropdown(config.models_available_metricas),
                    value=config.get_value_from_available_models(config.models_available_metricas),
                    style={'position':'relative', 'zIndex':'999'}
                ),
                html.Label('Seleccione el Threshold'),
                html.Div(children=[
                    html.Div(children=[
                        dcc.Input(
                            id='threshold_input',
                            type='number',
                            value=0.5,
                        ),
                    ], className="six columns"),
                    html.Div([
                        html.Button(children=['Aplicar mejor Threshold'], id = 'best_threshold_button', style={'backgroundColor':'#889EB5', 'color': 'white'}),
                        html.Div("Aplicar el botón azul de mejor threshold selecciona el mejor threshold encontrado por el índice de Jouden"),
                    ], className="six columns"),
                ], className="row"),
                dcc.Slider(
                    id="threshold_slider",
                    min=0,
                    max=1,
                    step=0.01,
                    value=0.5,
                ),
                html.Div(id='slider-output-container'),
            ], className="six columns"),
        ], className = "row"),
        html.Div(children=[
            html.H4('Búsqueda individual:', className="twelve columns"),
            html.Div(children=[
                html.Label('Escribe el ID del estudiante cuya resultado quieres ver para la materia, modelo y semestre actuales:'),
                dcc.Input(
                    id='id_student',
                    type='number',
                    value=1
                ),
                html.Div(children=[
                    dash_table.DataTable(
                    id='table_individual_student',
                    columns=data.get_random_cols(),
                    data=data.get_random_data(),
                    n_fixed_rows=1,
                    style_table={
                        'overflowX': 'scroll',
                        'overflowY': 'scroll',
                        'maxHeight': '200px',
                        'border': 'thin lightgrey solid',
                    },
                    style_cell={
                        'width': '200px',
                        'whiteSpace': 'normal'
                    },
                    style_header={
                        'backgroundColor': 'white',
                        'fontWeight': 'bold'
                    },
                    style_cell_conditional=[{
                        'if': {'row_index': 'odd'},
                        'backgroundColor': 'rgb(248, 248, 248)'
                    }],
                    css=[{
                        'selector': '.dash-cell div.dash-cell-value',
                        'rule': 'display: inline; white-space: inherit; overflow: inherit; text-overflow: inherit;'
                    }]
                    )
                ],className="twelve columns")
                
                
            ],className="row"),
        ], className="row"),

        html.Div(children=[
            html.H4('Estudiantes que reprueban:',id='number_of_students', className="twelve columns"),

            html.Div(children=[
                dash_table.DataTable(
                id='table_filtered',
                columns=data.get_random_cols(),
                data=data.get_random_data(),
                n_fixed_rows=1,
                style_table={
                    'overflowX': 'scroll',
                    'overflowY': 'scroll',
                    'maxHeight': '400px',
                    'border': 'thin lightgrey solid',
                },
                style_cell={
                    'width': '200px',
                    'whiteSpace': 'normal'
                },
                style_header={
                    'backgroundColor': 'white',
                    'fontWeight': 'bold'
                },
                style_cell_conditional=[{
                    'if': {'row_index': 'odd'},
                    'backgroundColor': 'rgb(248, 248, 248)'
                }],
                css=[{
                    'selector': '.dash-cell div.dash-cell-value',
                    'rule': 'display: inline; white-space: inherit; overflow: inherit; text-overflow: inherit;'
                }]
                )
            ], className="twelve columns"),
        ], className="row"),

        html.Div(children=[
            dcc.ConfirmDialogProvider(children=html.Button('Generar CSV con estudiantes con alta probabilidad de reprobar',
                id='generate_file_button', className="six columns", title="botón de descarga", style={'backgroundColor':'#889EB5', 'color': 'white'} ),
                id='generate_new_link_provider',
                message="""El CSV con los estudiantes en riesgo ha sido generado.
                Presiona el botón azul descargar CSV para descargar el archivo en su equipo local"""),
            html.A(html.Button(children=['Descargar CSV generado'], id = 'boton_download', style={'backgroundColor':'#119DFF', 'color': 'white'}),
            id='my_link', className="six columns"),
        ], className="row"),

        

        html.Div(children=[
            html.H4('Todos los estudiantes:', id='number_of_students_all_grades' , className="twelve columns"),

            html.Div(children=[
                dash_table.DataTable(
                id='table',
                columns=data.get_random_cols(),
                data=data.get_random_data(),
                n_fixed_rows=1,
                style_table={
                    'overflowX': 'scroll',
                    'overflowY': 'scroll',
                    'maxHeight': '400px',
                    'border': 'thin lightgrey solid',
                },
                style_cell={
                    'width': '200px',
                    'whiteSpace': 'normal'
                },
                style_header={
                    'backgroundColor': 'white',
                    'fontWeight': 'bold'
                },
                style_cell_conditional=[{
                    'if': {'row_index': 'odd'},
                    'backgroundColor': 'rgb(248, 248, 248)'
                }],
                css=[{
                    'selector': '.dash-cell div.dash-cell-value',
                    'rule': 'display: inline; white-space: inherit; overflow: inherit; text-overflow: inherit;'
                }]
                )
            ], className="twelve columns"),

        ], className="row"),

        html.Div(children=[
            html.H4('Histograma con probabilidad de reprobar' , className="twelve columns"),

        ], className="row"),        

        html.Div(children=[
            html.Div(children=[
                dcc.Graph(
                    id='probabilities_histogram',
                    figure=serve_random_figure(),
                ),
            ], className="twelve columns"),
        ], className="Row"),
        html.Div(children=[
            html.Div(
                style={
                    'min-width': '24.5%',
                    'height': 'calc(100vh - 90px)',
                    'margin-top': '5px',

                    # Remove possibility to select the text for better UX
                    'user-select': 'none',
                    '-moz-user-select': 'none',
                    '-webkit-user-select': 'none',
                    '-ms-user-select': 'none'
                },
            children=[
                html.H4(children=["Muestra de estudiantes seleccionada"]),
                dcc.Graph(
                    id='graph_heat_confusion_matrix',
                    figure=serve_random_figure(),
                    style={'height': '60%'}
                ),
                dcc.Graph(
                    id='graph_pie_confusion_matrix',
                    figure=serve_random_figure(),
                    style={'height': '60%'}
                ),
                dcc.Graph(
                    id='graph_line_roc_curve',
                    figure=serve_random_roc_curve(),
                    style={'height': '40%'}
                ),
                dcc.Graph(
                    id='graph_precision_recall_curve',
                    figure=serve_random_figure(),
                    style={'height': '40%'}
                ),
                html.Div(children=[
                    html.H4(children=["KS test"]),
                    html.P(children=[
                        """El KS test o  Kolmogorov-Smirnov statistic
                        test prueba si 2 muestras se extraen de la misma distribución.
                        Si el estadístico KS es pequeño o el p-value es alto, no podemos
                        rechazar la hipótesis de que las distribuciones de las dos muestras
                        son las mismas."""
                    ]),
                    html.H5('Estadístico'),
                    html.H6(id='ks_test_statistic'),
                    html.H5('P-value'),
                    html.H6(id='ks_test_p_value'),

                ])
            ], className="six columns"),
            html.Div(
                style={
                    'min-width': '24.5%',
                    'height': 'calc(100vh - 90px)',
                    'margin-top': '5px',

                    # Remove possibility to select the text for better UX
                    'user-select': 'none',
                    '-moz-user-select': 'none',
                    '-webkit-user-select': 'none',
                    '-ms-user-select': 'none'
                },
                children=[
                html.H4(children=["Modelo"]),
                dcc.Graph(
                    id='graph_heat_confusion_matrix_model',
                    figure=serve_random_figure(),
                    style={'height': '60%'}
                ),
                dcc.Graph(
                    id='graph_pie_confusion_matrix_model',
                    figure=serve_random_figure(),
                    style={'height': '60%'}
                ),
                dcc.Graph(
                    id='graph_line_roc_curve_model',
                    figure=serve_random_figure(),
                    style={'height': '40%'}
                ),
                dcc.Graph(
                    id='graph_precision_recall_curve_model',
                    figure=serve_random_figure(),
                    style={'height': '40%'}
                ),
                html.Div(children=[
                    html.H4(children=["KS test modelo completo"]),
                    html.P(children=[
                        """El KS test o  Kolmogorov-Smirnov statistic
                        test prueba si 2 muestras se extraen de la misma distribución.
                        Si el estadístico KS es pequeño o el p-value es alto, no podemos
                        rechazar la hipótesis de que las distribuciones de las dos muestras
                        son las mismas."""
                    ]),
                    html.H5('Estadístico'),
                    html.H6(id='ks_test_statistic_model'),
                    html.H5('P-value'),
                    html.H6(id='ks_test_p_value_model'),

                ])
            ], className="six columns"),
        ], className="row"),

    ])    
])


# callbacks



# input threshold
# dash.dependencies.State('threshold_slider', 'value'),
@app.callback(
    dash.dependencies.Output('threshold_input', 'value'),
    [dash.dependencies.Input('best_threshold_button', 'n_clicks')],
    [dash.dependencies.State('model_selector', 'value'),
    dash.dependencies.State('class_name_selector', 'value'),
    dash.dependencies.State('semester_selector', 'value')])
def update_value_slider(n_clicks, model, class_name, semester):
    data_table = data_tables[model]
    y_test, y_pred = data.get_y_test_pred_2(data_table, model, class_name, semester)
    best_threshold = statistics.cutoff_youdens_j(y_pred, y_test)
    return min(float(best_threshold), 1)

# slider
@app.callback(
    dash.dependencies.Output('slider-output-container', 'children'),
    [dash.dependencies.Input('threshold_slider', 'value')])
def update_output_threshold(value):
    return """El threshold de probabilidad para perder es "{}".
      Se seleccionarán los estudiantes con una probabilidad
      de perder superior al threshold.
     """.format(value)


@app.callback(
    dash.dependencies.Output('threshold_slider', 'value'),
    [dash.dependencies.Input('threshold_input', 'value')])
def update_value_slider(value):
    return min(float(value), 1)

# individual search
# filtered data 
@app.callback(
    [dash.dependencies.Output('table_individual_student', 'columns'),
    dash.dependencies.Output('table_individual_student', 'data'),],
    [dash.dependencies.Input('id_student', 'value'),
    dash.dependencies.Input('semester_selector', 'value'),
    dash.dependencies.Input('class_name_selector', 'value'),
    dash.dependencies.Input('model_selector', 'value'),])
def update_individual_student_result(id_student, semester, class_name, model):
    data_table = data_tables[model]
    filtered_data = data.get_data_for_single_student(data_table, id_student, semester, class_name)
    human_cols_in_order, data_with_columns_in_order = data.select_only_available_cols_v2(filtered_data, config.app_metricas, model)
    data_table_cols = [{"name": i["human_name"], "id": i["id"]} for i in human_cols_in_order]
    return data_table_cols, data_with_columns_in_order.to_dict("rows")


@app.callback(
    [dash.dependencies.Output('number_of_students', 'children'),
    dash.dependencies.Output('table_filtered', 'columns'),
    dash.dependencies.Output('table_filtered', 'data'),],
    [dash.dependencies.Input('threshold_slider', 'value'),
    dash.dependencies.Input('model_selector', 'value'),
    dash.dependencies.Input('class_name_selector', 'value'),
    dash.dependencies.Input('semester_selector', 'value'),])
def update_number_students_and_filter_data_table_metricas(threshold, model, class_name, semester):
    data_table = data_tables[model]
    filtered_data = data.filter_student_data(data_table, threshold, model, class_name, semester)
    human_cols_in_order, data_with_columns_in_order = data.select_only_available_cols_v2(filtered_data, config.app_metricas, model)
    number = data_with_columns_in_order.shape[0]
    number_message = 'Estudiantes que reprueban: ' + str(number)
    data_rows = data_with_columns_in_order.to_dict("rows")
    data_table_cols = [{"name": i["human_name"], "id": i["id"]} for i in human_cols_in_order]
    return  number_message,data_table_cols, data_rows


@app.callback(
    [dash.dependencies.Output('number_of_students_all_grades', 'children'),
    dash.dependencies.Output('table', 'columns'),
    dash.dependencies.Output('table', 'data'),],
    [dash.dependencies.Input('model_selector', 'value'),
    dash.dependencies.Input('class_name_selector', 'value'),
    dash.dependencies.Input('semester_selector', 'value'),])
def update_all_grades_metricas(model, class_name, semester):
    data_table = data_tables[model]
    filtered_data = data.filter_student_data_all_grades(data_table, class_name, semester)
    human_cols_in_order, data_with_columns_in_order = data.select_only_available_cols_v2(filtered_data, config.app_metricas, model)
    number = data_with_columns_in_order.shape[0]
    number_message = 'Todos los estudiantes: ' + str(number)
    data_rows = data_with_columns_in_order.to_dict("rows")
    data_table_cols = [{"name": i["human_name"], "id": i["id"]} for i in human_cols_in_order]
    return  number_message,data_table_cols, data_rows



@app.callback(
    dash.dependencies.Output('probabilities_histogram', 'figure'),
    [dash.dependencies.Input('semester_selector', 'value'),
    dash.dependencies.Input('class_name_selector', 'value')])
def update_histogram(semester, class_name):
    filtered_tables = {}
    for model, student_data_table in data_tables.items():
        filtered_data = data.filter_student_data_all_grades(student_data_table, class_name, semester)
        filtered_tables[model] = filtered_data
    histogram = serve_histogram_v2(filtered_tables, config.app_metricas)
    return histogram


# updates confusion matrix
@app.callback(
    [dash.dependencies.Output('graph_pie_confusion_matrix', 'figure'),
    dash.dependencies.Output('graph_heat_confusion_matrix', 'figure')],
    [dash.dependencies.Input('threshold_slider', 'value'),
    dash.dependencies.Input('model_selector', 'value'),
    dash.dependencies.Input('class_name_selector', 'value'),
    dash.dependencies.Input('semester_selector', 'value'),])
def update_confusion_matrix(threshold, model, class_name, semester):
    data_table = data_tables[model]
    y_test, y_pred = data.get_y_test_pred(data_table, threshold, model, class_name, semester)
    confusion_figure_heat = serve_heatmap_confusion_matrix(
        y_test=y_test,
        y_pred=y_pred, 
        threshold = threshold
    )
    confusion_figure_pie = serve_pie_confusion_matrix(
        y_test=y_test,
        y_pred=y_pred, 
        threshold = threshold
    )
    return confusion_figure_pie, confusion_figure_heat

# updates roc curve and auc
@app.callback(
    dash.dependencies.Output('graph_line_roc_curve', 'figure'),
    [dash.dependencies.Input('threshold_slider', 'value'),
    dash.dependencies.Input('model_selector', 'value'),
    dash.dependencies.Input('class_name_selector', 'value'),
    dash.dependencies.Input('semester_selector', 'value'),])
def update_roc_curve(threshold, model, class_name, semester):
    data_table = data_tables[model]
    y_test, y_pred = data.get_y_test_pred(data_table, threshold, model, class_name, semester)
    roc_curve = serve_roc_curve(
        y_test=y_test,
        y_pred=y_pred) 
    return roc_curve

# updates precision recall curve
@app.callback(
    dash.dependencies.Output('graph_precision_recall_curve', 'figure'),
    [dash.dependencies.Input('threshold_slider', 'value'),
    dash.dependencies.Input('model_selector', 'value'),
    dash.dependencies.Input('class_name_selector', 'value'),
    dash.dependencies.Input('semester_selector', 'value'),])
def update_precision_recall_curve(threshold, model, class_name, semester):
    data_table = data_tables[model]
    y_test, y_pred = data.get_y_test_pred(data_table, threshold, model, class_name, semester)
    precision_recall_figure = serve_precision_recall_curve(
        y_test=y_test,
        y_pred=y_pred,
        threshold=threshold) 
    return precision_recall_figure


# updates ks test
@app.callback(
    [dash.dependencies.Output('ks_test_statistic', 'children'),
    dash.dependencies.Output('ks_test_p_value', 'children')],
    [dash.dependencies.Input('model_selector', 'value'),
    dash.dependencies.Input('class_name_selector', 'value'),
    dash.dependencies.Input('semester_selector', 'value'),])
def update_ks_test(model, class_name, semester):
    data_table = data_tables[model]
    model_probabilities, marca = data.get_column_model_and_reality(data_table, model, class_name, semester)
    statistic, p_value = statistics.get_ks_test_2samp(model_probabilities, marca)
    s = ("{0:.3f}".format(statistic) , "{0:.3f}".format(p_value))
    return s



# updates model figures
@app.callback(
    [dash.dependencies.Output('graph_heat_confusion_matrix_model', 'figure'),
    dash.dependencies.Output('graph_pie_confusion_matrix_model', 'figure'),
    dash.dependencies.Output('graph_line_roc_curve_model', 'figure'),
    dash.dependencies.Output('graph_precision_recall_curve_model', 'figure'),
    dash.dependencies.Output('ks_test_statistic_model', 'children'),
    dash.dependencies.Output('ks_test_p_value_model', 'children'),],
    [dash.dependencies.Input('model_selector', 'value')])
def update_model_figures(model):
    hmcm = figures_for_models[model]["hmcm"]
    gpcm =  figures_for_models[model]["gpcm"]
    roc =  figures_for_models[model]["roc"]
    pr =  figures_for_models[model]["pr"]
    ks = figures_for_models[model]["ks"]
    t = (hmcm,gpcm,roc,pr, ks[0], ks[1])
    return t


# boton descarga CSV


@app.callback(
    Output('my_link', 'href'),
    [dash.dependencies.Input('generate_new_link_provider', 'submit_n_clicks')],
    [dash.dependencies.State('threshold_slider', 'value'),
    dash.dependencies.State('model_selector', 'value'),
    dash.dependencies.State('class_name_selector', 'value'),
    dash.dependencies.State('semester_selector', 'value')])
def update_link(n_clicks, threshold, model, class_name, semester):
    value = str(threshold) + "$" + str(model) + "$" + str(class_name) + "$" + str(semester)
    urlSafeEncodedBytes = base64.urlsafe_b64encode(value.encode("utf-8"))
    urlSafeEncodedStr = str(urlSafeEncodedBytes, "utf-8")
    link_to_return = '/dash/urlToDownloadMetricas?value={}'.format(urlSafeEncodedStr)
    return link_to_return


@app.server.route('/dash/urlToDownloadMetricas') 
def download_csv():
    value = flask.request.args.get('value')
    # create a dynamic csv or file here using `StringIO` 
    # (instead of writing to the file system)
    value_decoded = base64.urlsafe_b64decode(value).decode("utf-8", "ignore") 
    params = value_decoded.split("$")
    threshold = float(params[0])
    model = params[1]
    class_name = params[2]
    semester = params[3]
    data_table = data_tables[model]
    filtered_data = data.filter_student_data(data_table, threshold, model, class_name, semester)
    str_io = io.StringIO()
    filtered_data.to_csv(str_io)
    mem = io.BytesIO()
    mem.write(str_io.getvalue().encode('utf-8'))
    mem.seek(0)
    str_io.close()
    now = datetime.now()
    d = now.strftime("%d-%m-%Y-%H:%M:%S")
    return flask.send_file(mem,
                     mimetype='text/csv',
                     attachment_filename= config.prefijo_csv_archivo_metricas + "-" + d +'.csv',
                     as_attachment=True)


@app.callback(
    dash.dependencies.Output('boton_download', 'children'),
    [dash.dependencies.Input('generate_new_link_provider', 'submit_n_clicks')],)
def update_output_button(n_clicks):
    now = datetime.now()
    d = now.strftime("%d-%m-%Y-%H:%M:%S")
    return 'Descargar CSV generado a las ' + d