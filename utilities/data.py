import pandas as pd
import config
import collections

data = collections.OrderedDict(
    [
        ("Date", ["2015-01-01", "2015-10-24", "2016-05-10", "2017-01-10", "2018-05-10", "2018-08-15"]),
        ("Region", ["Montreal", "Toronto", "New York City", "Miami", "San Francisco", "London"]),
        ("Temperature", [1, -20, 3.512, 4, 10423, -441.2]),
        ("Humidity", [10, 20, 30, 40, 50, 60]),
        ("Pressure", [2, 10924, 3912, -10, 3591.2, 15]),
    ]
)

df = pd.DataFrame(data)

def get_random_cols():
    columns=[{'id': c, 'name': c} for c in df.columns]
    return columns

def get_random_data():
    data=df.to_dict('records')
    return data

def get_filter_column(model):
    """Returns the name of the column that should be used to filter
    the data given the model """
    filter_column = "probabilidad_reprobar_exante"
    if model == config.ex_ante:
        filter_column = "probabilidad_reprobar_exante"
    elif model == config.ex_post_p1:
        filter_column = "probabilidad_reprobar_expost_p1"
    elif model == config.ex_post_p1_p2:
        filter_column = "probabilidad_reprobar_expost_p1_p2"
    else:
        print("Selector model is not associated to any columns")
    return filter_column

def filter_student_data(student_data, threshold, model, class_name, semester):
    """ returns the data filtered by the threshold and model"""
    filter_column = get_filter_column(model)
    filtered_data = student_data[student_data[filter_column] > threshold]
    filtered_data =  filtered_data[filtered_data[config.class_column_name] == class_name]
    filtered_data =  filtered_data[filtered_data[config.semester_column_name] == int(semester)]
    return filtered_data

def filter_student_data_no_threshold(student_data, class_name, semester):
    """ returns the data filtered by class_name and semester"""
    filtered_data =  student_data[student_data[config.class_column_name] == class_name]
    filtered_data =  filtered_data[filtered_data[config.semester_column_name] == int(semester)]
    return filtered_data

def filter_student_data_all_grades(student_data, class_name, semester):
    """ returns the data filtered by the and model, class and semester"""
    filtered_data =  student_data[student_data[config.class_column_name] == class_name]
    filtered_data =  filtered_data[filtered_data[config.semester_column_name] == int(semester)]
    return filtered_data

def get_data_for_single_student(student_data, id_student, semester, class_name):
    """ returns the data for a single student given the id of the student and the semester"""
    filter_condition = (student_data[config.id_column_name]==id_student) & (student_data[config.semester_column_name]== int(semester)) & (student_data[config.class_column_name]== class_name)
    student_row = student_data[filter_condition]
    return student_row

def get_y_test_pred(student_data, threshold, model, class_name, semester):
    """ returns y_test and the y_pred for given data, threshold, model, class and semester"""
    filter_column = get_filter_column(model)
    filtered_data = filter_student_data(student_data, threshold, model, class_name, semester)
    y_test = filtered_data[config.marca_column_name]
    y_pred = filtered_data[filter_column]
    return (y_test, y_pred)

def get_y_test_pred_2(student_data, model, class_name, semester):
    """ returns y_test and the y_pred for given data, threshold, model, class and semester"""
    filter_column = get_filter_column(model)
    filtered_data = filter_student_data_no_threshold(student_data, class_name, semester)
    y_test = filtered_data[config.marca_column_name]
    y_pred = filtered_data[filter_column]
    return (y_test, y_pred)
 
def check_y_test_ypred_length(y_pred, y_test):
    len_y_pred = len(y_pred)
    len_y_test = len(y_pred)
    check_same_size = len_y_pred == len_y_test
    check_larger_than_0 = len_y_pred > 0 and len_y_test > 0
    return check_same_size, check_larger_than_0

def data_length_correct(y_pred, y_test):
    """Checks if the arrays have the correct length """
    error_message = None
    check_same_size, check_larger_than_0 = check_y_test_ypred_length(y_pred, y_test)
    if not check_same_size:
        error_message = get_error_message_for_length_problems(config.check_type_errors["different_length"]["value"])
        return False, error_message
    elif not check_larger_than_0:
        error_message = get_error_message_for_length_problems(config.check_type_errors["one_vector_has_length_zero"]["value"])
        return False, error_message
    else:
        return True, error_message

def get_error_message_for_length_problems(check_type):
    error_message = None
    try:
        error_message = config.check_type_errors[check_type]["message"]
    except KeyError:
        raise ValueError("received check_type not defined in config.py")
    return error_message
        

def get_column_model_and_reality(student_data, model, class_name, semester):
    """ returns the column with the data and the marca for a particular model"""
    filtered_data = student_data[student_data[config.semester_column_name] == int(semester)]
    filter_column = get_filter_column(model)
    column = list(filtered_data[filter_column])
    marcas = list(filtered_data[config.marca_column_name])
    return (column, marcas)

def order_table_columns(cols, student_data, app_name):
    models_available = None
    if app_name == config.app_metricas:
        models_available = config.models_available_metricas
    elif app_name == config.app_alertas:
        models_available = config.models_available_alertas
    else:
        print("app name is wrong")
    
    cols_to_order = config.basic_cols_to_keep
    for model in models_available:
        cols_to_check_in_order = config.model_column_map[model]["cols_to_check"]
        for col in cols_to_check_in_order:
            if col not in cols_to_order:
                cols_to_order.append(col)

    data_with_columns_in_order = student_data[cols_to_order]
    return cols_to_order, data_with_columns_in_order

def select_only_available_cols(student_data, app_name):
    models_available = None
    if app_name == config.app_metricas:
        models_available = config.models_available_metricas
    elif app_name == config.app_alertas:
        models_available = config.models_available_alertas
    else:
        print("app name is wrong")
    
    cols_to_order = list(config.basic_cols_to_keep)
    for model in models_available:
        cols_to_check_in_order = config.model_column_map[model]["cols_to_check"]
        for col in cols_to_check_in_order:
            if col not in cols_to_order:
                cols_to_order.append(col)

    try:
        data_with_columns_in_order = student_data[cols_to_order]
    except KeyError as ke:
        print("""The app tried to filter the data for columns not available. Please check
        columns in dataset or cols_to_order""")
        raise ke
    human_cols_in_order = change_column_names(cols_to_order)
    return human_cols_in_order, data_with_columns_in_order


def select_only_available_cols_v2(student_data, app_name, model):
    cols_to_order = config.model_column_map[model]["cols_available"]
    data_with_columns_in_order = student_data[cols_to_order]
    try:
        data_with_columns_in_order = student_data[cols_to_order]
    except KeyError as ke:
        print("""The app tried to filter the data for columns not available. Please check
        columns in dataset or cols_to_order""")
        raise ke
    human_cols_in_order = change_column_names(cols_to_order)
    return human_cols_in_order, data_with_columns_in_order

def change_column_names(column_names):
    """Given a list with column names, returns a list with human
    column names """
    human_column_names = [{ "human_name": config.human_column_names[col], "id": col} for col in column_names]
    return human_column_names

