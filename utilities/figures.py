import colorlover as cl
import plotly.graph_objs as go
import numpy as np
from sklearn import metrics

import utilities.data as data_functions
import config


def serve_histogram_v2(data_tables, app_name):
    if app_name == config.app_alertas:
        models_available = config.models_available_alertas
    elif app_name == config.app_metricas:
        models_available = config.models_available_metricas
    else:
        raise ValueError("The name of the application is not defined in the config file")
    columns, names = config.get_columns_for_available_models(models_available)
    traces = []
    for model in models_available:
        column = data_functions.get_filter_column(model)
        traces.append(list(data_tables[model][column]))
    histogram = serve_overlaid_histogram(traces, names)
    return histogram

def serve_histogram(student_data, app_name):
    if app_name == config.app_alertas:
        models_available = config.models_available_alertas
    elif app_name == config.app_metricas:
        models_available = config.models_available_metricas
    else:
        raise ValueError("The name of the application is not defined in the config file")
    columns, names = config.get_columns_for_available_models(models_available)
    traces = [ list(student_data[col]) for col in columns]
    histogram = serve_overlaid_histogram(traces, names)
    return histogram

def serve_overlaid_histogram(traces, names):
    data = []
    for index, trace in enumerate(traces):
        trace_i = go.Histogram(
            name=names[index],
            x=trace,
            opacity=0.60,
            nbinsx=20,
        )
        data.append(trace_i)
    layout = go.Layout(
        barmode='overlay',
        xaxis=dict(
            title='Probability'
        ),
        yaxis=dict(
            title='Number of students'
        ),
        )
    figure = go.Figure(data=data, layout=layout)
    return figure


def serve_random_figure():
    traces = [[0.1,0.2,0.3], [0.1,0.2,0.3], [0.1,0.2,0.3]]
    data = []
    for trace in traces:
        trace_i = go.Histogram(
            x=trace,
            opacity=0.60,
            nbinsx=10,
        )
        data.append(trace_i)
    layout = go.Layout(barmode='overlay')
    figure = go.Figure(data=data, layout=layout)
    return figure

def serve_roc_curve(y_pred,
                    y_test):
    data_length_correct_bool, error_message = data_functions.data_length_correct(y_pred, y_test)
    if not data_length_correct_bool:
        return serve_empty_figure("roc", error_message)
    else:
        fpr, tpr, threshold = metrics.roc_curve(y_test, y_pred)
        # AUC Score
        title_g = ""
        if len(set(y_test)) < 2:
            auc_score = "No se puede calcular AUC porque toda la marca es 1"
            title_g = 'ROC Curve (AUC =' + auc_score+ ')'
        else:
            auc_score = metrics.roc_auc_score(y_true=y_test, y_score=y_pred)
            title_g = f'ROC Curve (AUC = {auc_score:.3f}))'

        trace0 = go.Scatter(
            x=fpr,
            y=tpr,
            mode='lines',
            name='Test Data',
        )

        trace1 = go.Scatter(
            x=np.linspace(0,1,100,endpoint=False),
            y=np.linspace(0,1,100,endpoint=False),
            mode='lines',
            name='Chance level',
        )

        layout = go.Layout(
            title=title_g,
            xaxis=dict(
                title='False Positive Rate'
            ),
            yaxis=dict(
                title='True Positive Rate'
            ),
            legend=dict(x=0, y=1.05, orientation="h"),
            margin=dict(l=50, r=10, t=55, b=40),
        )

        data = [trace0, trace1]
        figure = go.Figure(data=data, layout=layout)

        return figure

    
    

def serve_random_roc_curve():
    y_test = [1,1,0,1]
    y_pred = [0,1,0,1]
    fpr, tpr, threshold = metrics.roc_curve(y_test, y_pred)
    # AUC Score
    auc_score = metrics.roc_auc_score(y_true=y_test, y_score=y_pred)

    trace0 = go.Scatter(
        x=fpr,
        y=tpr,
        mode='lines',
        name='Test Data',
    )

    layout = go.Layout(
        title=f'ROC Curve (AUC = {auc_score:.3f})',
        xaxis=dict(
            title='False Positive Rate'
        ),
        yaxis=dict(
            title='True Positive Rate'
        ),
        legend=dict(x=0, y=1.05, orientation="h"),
        margin=dict(l=50, r=10, t=55, b=40),
    )

    data = [trace0]
    figure = go.Figure(data=data, layout=layout)

    return figure

def serve_heatmap_confusion_matrix(y_test,
                               y_pred, 
                               threshold):
    data_length_correct_bool, error_message = data_functions.data_length_correct(y_pred, y_test)
    if not data_length_correct_bool:
        return serve_empty_figure("Confusion Matrix", error_message)
    else:
        y_pred_test = [1 if y > threshold else 0 for y in y_pred]
        matrix = metrics.confusion_matrix(y_true=y_test, y_pred=y_pred_test)
        result = matrix.ravel()
        if len(result) != 4 and len(result) ==1:
            tn, fp, fn, tp = 0, 0, 0, result[0]
        else:
            tn, fp, fn, tp = result
        trace = go.Heatmap(x= ["Aprobo", "No Aprobo"], 
          y=["No Aprobo", "Aprobo"], 
          z=[[tp, fp],[fn, tn]], 
          colorscale="Reds")
        data = [trace]
        layout = {
          "annotations": [
            {
              "x": "Aprobo", 
              "y": "Aprobo", 
              "font": {"color": "white"}, 
              "showarrow": False, 
              "text": str(tp), 
              "xref": "x1", 
              "yref": "y1"
            }, 
            {
              "x": "No Aprobo", 
              "y": "Aprobo", 
              "font": {"color": "white"}, 
              "showarrow": False, 
              "text": str(fp), 
              "xref": "x1", 
              "yref": "y1"
            }, 
            {
              "x": "Aprobo", 
              "y": "No Aprobo", 
              "font": {"color": "white"}, 
              "showarrow": False, 
              "text": str(fn), 
              "xref": "x1", 
              "yref": "y1"
            }, 
            {
              "x": "No Aprobo", 
              "y": "No Aprobo", 
              "font": {"color": "white"}, 
              "showarrow": False, 
              "text": str(tn), 
              "xref": "x1", 
              "yref": "y1"
            }, 
          ], 
          "title": "Confusion Matrix", 
          "xaxis": {"title": "Real Value"}, 
          "yaxis": {"title": "Predicted Value"}
        }
        figure = go.Figure(data=data, layout=layout)
        return figure
    


def serve_pie_confusion_matrix(y_test,
                               y_pred, 
                               threshold):
    data_length_correct_bool, error_message = data_functions.data_length_correct(y_pred, y_test)
    if not data_length_correct_bool:
        return serve_empty_figure("Confusion Matrix (Pie Chart)", error_message)
    else:
        # Compute y_pred using threshold
        y_pred_test = [1 if y > threshold else 0 for y in y_pred]
        matrix = metrics.confusion_matrix(y_true=y_test, y_pred=y_pred_test)
        result = matrix.ravel()
        if len(result) != 4 and len(result) ==1:
            tn, fp, fn, tp = 0, 0, 0, result[0]
        else:
            tn, fp, fn, tp = result

        values = [tp, fn, fp, tn]
        label_text = ["True Positive",
                      "False Negative",
                      "False Positive",
                      "True Negative"]
        labels = ["TP", "FN", "FP", "TN"]
        blue = cl.flipper()['seq']['9']['Blues']
        red = cl.flipper()['seq']['9']['Reds']
        colors = [blue[4], blue[1], red[1], red[4]]

        trace0 = go.Pie(
            labels=label_text,
            values=values,
            hoverinfo='label+value+percent',
            textinfo='text+value',
            text=labels,
            sort=False,
            marker=dict(
                colors=colors
            )
        )

        layout = go.Layout(
            title=f'Confusion Matrix (Pie Chart)',
            margin=dict(l=10, r=10, t=60, b=10),
            legend=dict(
                bgcolor='rgba(255,255,255,0)',
                orientation='h'
            )
        )

        data = [trace0]
        figure = go.Figure(data=data, layout=layout)

        return figure
        
   

def serve_precision_recall_curve(y_test,y_pred,threshold):
    data_length_correct_bool, error_message = data_functions.data_length_correct(y_pred, y_test)
    if not data_length_correct_bool:
        return serve_empty_figure("precision recall", error_message)
    else:
        precision, recall, _ = metrics.precision_recall_curve(y_true=y_test, probas_pred=y_pred)
        average_precision = metrics.average_precision_score(y_test, y_pred)
        trace0 = go.Scatter(
            x=recall,
            y=precision,
            mode='lines',
            name='Test Data',
        )

        trace1 = go.Scatter(
            x=np.linspace(0,1,100,endpoint=False),
            y=[average_precision] * 100,
            mode='text',
            text="-",
            name='Average',
        )

        layout = go.Layout(
            title=f'Precision-recall (Average precision= {average_precision:.3f}))',
            xaxis=dict(
                title='Recall'
            ),
            yaxis=dict(
                title='Precision'
            ),
            legend=dict(x=0, y=1.05, orientation="h"),
            margin=dict(l=50, r=10, t=55, b=40),
        )

        data = [trace0, trace1]
        figure = go.Figure(data=data, layout=layout)

        return figure


### serve empty figures in case there is no data
def serve_empty_figure(figure_type, error_message):
    """Serve empty figure with a particular error message """
    trace0 = go.Scatter(
        x=[1],
        y=[1],
        mode='lines',
        name='Test Data',
    )
    layout = go.Layout(
        title=figure_type,
        xaxis=dict(
            title='No hay datos o son incorrectos'
        ),
        yaxis=dict(
            title='No hay datos o son incorrectos'
        ),
        legend=dict(x=0, y=1.05, orientation="h"),
        margin=dict(l=50, r=10, t=55, b=40),
    )
    annotations = []

    annotations.append(dict(xref='paper', yref='paper', x=0.5, y=-0.1,
                              xanchor='center', yanchor='top',
                              text=error_message,
                              font=dict(family='Arial',
                                        size=12,
                                        color='rgb(150,150,150)'),
                              showarrow=False))
    layout['annotations'] = annotations
    data = [trace0]
    figure = go.Figure(data=data, layout=layout)
    return figure