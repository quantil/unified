from scipy import stats
import utilities.data as data_functions
from sklearn import metrics

def get_ks_test_2samp(samp_a, samp_b):
    """
    Compute the Kolmogorov-Smirnov statistic on 2 samples.
    Normally one sample is going to be the probability of 
    failing a class for one of the models and the other is
    going to be a vector with the actual results (0s and 1s)

    If the K-S statistic is small or the p-value is high, 
    then we cannot reject the hypothesis that the distributions
    of the two samples are the same.

    Returns:
            statistic: float
            KS statistic

            pvalue: float
            two-tailed p-value

    """
    data_length_correct_bool, error_message = data_functions.data_length_correct(samp_a, samp_b)
    if not data_length_correct_bool:
        return (error_message, "")
    else:
        test = stats.ks_2samp(samp_a, samp_b)
        return test
    
def cutoff_youdens_j(y_pred,
                y_test):
    data_length_correct_bool, error_message = data_functions.data_length_correct(y_pred, y_test)
    if not data_length_correct_bool:
        print("No se puede calcular")
        raise ValueError(error_message)
    else:
        fpr, tpr, thresholds = metrics.roc_curve(y_test, y_pred)
        j_scores = tpr-fpr
        j_ordered = sorted(zip(j_scores,thresholds))
        almost_optimal = j_ordered[-1][1]
        best_threshold = almost_optimal if almost_optimal <= 1 else almost_optimal - 1 
        return best_threshold
        